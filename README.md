CIS 322 Project 5
Author: Jacob Rammer - jrammer@uoregon.edu
This is an improved version of project 4 of brevet controle calculator. 

How to use: Enter all the information into the controle table, 
specify a brevet name and click submit to store data into the DB. 
To display previous submitted calculations click submit. 

Error handling / edge cases: 
Empty table or brevet name: redirected to error page
No data in DB: Display page with "no data to display"

Tests: wrote 2 test functions that are fed a status message 
from app.py and compares the message. Test case 1: empty table, test case 2: no data

